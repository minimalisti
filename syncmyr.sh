#!/bin/sh -x
rm -rf _production/
bundle exec jekyll build -d _production
echo 'Press ENTER...'
read
rsync -cprtvz --delete _production/ minimalisti@myrtana.sk:/home/minimalisti/public/
rm -rf _production/
