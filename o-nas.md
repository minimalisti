---
layout: page
title: O nás
permalink: /o-nas/
---

Ahoj!

Jsme Ivan a Michal, dva kamarádi, kteří vzali své životy do svých rukou, podívali se kolem sebe a začali přehodnocovat, co mají, co dělají&#160;– jestli to skutečně dává v&#160;jejich zamýšleném životě smysl.

Původně jsme své zkušenosti i nadšení z&#160;výsledků sdíleli jen mezi sebou, ale proč nešířit inspiraci i trochu dál. :·) Tenhle blog píšeme za sebe, nedáváme žádné zaručené rady, jen své pohledy, názory a zkušenosti.

K&#160;zastižení jsme na IRC (tj. on-line diskusi): [#minimalisti@libera.chat](https://web.libera.chat/) (odkaz vás dovede na webové rozhraní).
