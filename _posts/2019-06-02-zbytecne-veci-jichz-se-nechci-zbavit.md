---
title:  Zbytečné věci, jichž se nechci zbavit
author: Michal
image:  kindle3.jpeg
image-alt: Má čtečka Amazon Kindle 3
---
Při minimalizaci člověk vezme do ruky i věci, které roky nepoužil, které nepotřebuje, které mu jen zabírají místo a [zatěžují ho](/2019/01/pasivni-mentalni-zatez/). Přesto se jich v&#160;žádném případě nechce zbavit, nechce je prodat, ba ani darovat. Pořád pro něj totiž někde v&#160;hloubi něco znamenají, má s&#160;nimi spojené hezké chvíle a vzpomínky.

Minimalista ve mně velí zminimalizovat, a já se bráním, přestože vím, že má pravdu. Někdy stačí nechat věc odležet, vrátit se k&#160;ní později, leckdy ale ani to nestačí. Naprosto zřejmé to je u&#160;ryze osobních věcí, ale často se to týká i věcí běžnějšího charakteru. V&#160;tomto zápisku se věnuji těm druhým. A vezmu si na to konkrétní příklad z&#160;nedávné doby.

<figure class="post-image">
  <img src="/images/posts/kindle3.jpeg" alt="Má čtečka Amazon Kindle 3">
</figure>

Před dobrými 9 lety jsem si pořídil čtečku e-knížek Kindle 3 (dobrá věc). Za ty roky jsem se na ní něco načetl. Nejméně poslední 4 roky na ni však prakticky jen sedá prach. Baterka pomalu degraduje. Co tedy s&#160;ní.

Jak jsem naznačoval už v&#160;úvodu, ta čtečka se mi pořád líbí, přijde mi šikovná a nechci se jí vzdávat. Přestože mi k&#160;ničemu v&#160;současnosti není, přestože je stará, přestože jsem se od koupě docela vyhranil proti Amazonu, přestože nová by mě moc nestála, kdybych ji zase někdy chtěl využívat.

Ke svému překvapení jsem našel způsob, aby se minimalista nažral, a čtečka zůstala celá. *Čtečku jen půjčím.* Na dlouho. Na neurčito.

Bude to prakticky totéž, jako bych ji prodal nebo daroval, ale psychologicky je to daleko přijatelnější krok. Předpokládám, že si na to časem zvyknu, a nebude pak pro mě problém čtečku opustit nadobro.

Během promazávání čtečky jsem se už utvrdil v&#160;tom, že jde o&#160;správné rozhodnutí. A čtečka bude konečně zase někomu užitečná. Minimalismus vskutku přináší samé dobré pocity. :·)
