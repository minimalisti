---
title:  Neber veci zadarmo
author: Ivan
---
V&#160;súčasnosti sa nám doslova ponúka toľko vecí zadarmo, že v&#160;istom zmysle je to až podozrivé.

Aj na sebe pozorujem, ako ma strhávajú veci dostupné zadarmo. Rôzne reklamné predmety, jedlo zadarmo, časopisy zadarmo, rôzne promo akcie. Avšak aj keď sa nechám strhnúť vnútorne, navonok nerobím nič. Za chvíľu to zvykne prejsť, lebo si uvedomím, že to naozaj nepotrebujem.

Jasné, že cieľom týchto vecí je budovanie značky, reklama a propagácia, zaháčkovanie človeka či jeho informovanie. Alebo jedlo či veci zadarmo, lebo je to súčasťou nejakej udalosti ako konferencie.

Problémov je viac&#160;– zbaviť sa týchto vecí neskôr znamená ich v&#160;podstate vyhodiť do koša. Čo nie je ekologické a niekto strávil hodiny ich výrobou. A niekto strávi hodiny ich spálením alebo vyhodením na skládku. A to nevravím koľko sa spotrebovalo stromov či iných materiálov.

V&#160;prípade jedla zadarmo… v&#160;súčasnosti je jedlo veľmi lacné. Na rautoch sú kvantá jedla a treba sebauvedomenie a sebadisciplínu, aby sa človek neobžral.

Ale často mi niekto aj daruje jedlo alebo pozýva a nebolo to vždy jednoduché, lebo nemám už takú motiváciu jesť jedlo, o&#160;ktorom viem, že nie je pre mňa zdravé. Blbé je to, že je rozšírené hlavne nezdravé jedlo…

Tiež sa mi nepáči súčasný bežný prístup, že na prejav sympatií sa vyžaduje nutne vždy nejaký materiálny dar.

A obecne vidím mnoho zberačov, čo zbierajú všetko zadarmo. „Však je to zadarmo, nie?“ vravia. Tak zoberú časopis zadarmo v&#160;električke. Letáky a všelijaké blbosti. No, niektorí zoberú aj tie veci, čo nie sú pribité klincami. A mnohí si postavia väzenie z&#160;vecí.

Ako tiež som si tým všetkým prešiel, takže to úplne chápem. Ale je tu aj iná cesta, minimalizmus. Mať len to, čo človek potrebuje k&#160;životu.
