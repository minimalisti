---
title:  Hlavně to nepřehánět
author: Michal
---
Typické vnímání minimalismu mezi lidmi se dá připodobnit k&#160;antimaterialismu (nejspíš jen můj pojem), tedy nevlastnění věcí. Velice snadno k&#160;tomu ale může sklouznout i leckterý minimalista.

Opouštění věcí, zbavování se jich (osobně tomu říkám „proces minimalizace“) vyvolává [pozitivní pocity](/2019/01/pasivni-mentalni-zatez/), které vedou k&#160;tomu, že člověk v&#160;minimalizaci s&#160;rostoucím nadšením pokračuje. A na to pozor&#160;– nevytvořme si z&#160;minimalizace drogu.

Jedním extrémem je pořizování si kvanta věcí, protože z&#160;nich člověk má dobrý pocit. Druhým podobně chorobné zbavování se (skoro) všeho.

**Smyslem minimalismu *není* nemít věci. Smyslem je mít (jen) věci, které v&#160;našem životě dávají smysl, které pro nás mají význam a hodnotu, které náš život nějakým způsobem obohacují.** Tohle bychom měli mít stále na paměti.

Je totiž naprosto v&#160;pořádku mít věci! Ani během minimalizace není nutné hned vše vyhodit&#160;– pokud si nejsem jistý, jestli už něco skutečně nechci, tak to nechám odležet. Až se mi to dostane do ruky příště, můžu mít na věc nový pohled a rozhodnu se snadněji (je sice pravděpodobnější, že takovouto věc opustím, ale není třeba z&#160;toho dělat bolestivý proces).

A právě v&#160;tomto rozhodování mi minimalismus výrazně pomohl&#160;– zatímco dříve jsem takřka všechno ukládal zpátky tam, kde to bylo, dnes umím rozlišit podstatné od zbytečného a to zbytečné opustit. Výsledky už začínají být i vidět a mám z&#160;nich radost. Pořád mám hromadu věcí, ale z&#160;těchto, které jsem se rozhodl si nechat, se těším podstatně více, než když jsem o&#160;minimalismu ještě nevěděl.
