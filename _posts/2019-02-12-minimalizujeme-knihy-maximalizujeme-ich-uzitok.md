---
title:  Minimalizujeme knihy, maximalizujeme ich úžitok
author: Ivan
---
Knihy. Knihy sú super. Milujem knihy. Preto je dôležité zachovať si k&#160;nim čerstvý a zdravý vzťah.

Ešte rok dozadu som si myslel, že knihy u&#160;mňa skončili&#160;– nevedel som v&#160;nich nájsť ani potešenie z&#160;ničoho nového a ani potešenie z&#160;nových znalostí.

Na vyriešenie sa mi osvedčil minimalistický prístup. Za 7 rokov som si vybudoval svoju osobnú knižnicu, ktorá sa mi ale stala po čase väzením. Uvedomil som si, že je pre mňa dôležitejšie čítať rôzne knihy, než nejaké vlastniť. Preto som sa svojej knižnice zbavil a teraz viac spolieham na verejné knižnice, súkromné knižnice ľudí, čo poznám, a Internet.

Knihy som začal kupovať, hlavne lebo som sa chcel vykúpiť z&#160;toho, že som ich kedysi stiahol na internete a čítal na počítači. Aj keď teraz sa doba zmenila, vtedy bolo fantasy a sci-fi literatúry v mestskej knižnici málo a sám som ako dieťa nemal finančné zdroje na kupovanie si kníh.

Iné hlavné dôvody boli nuda a následná návšteva Levných kníh, a&#160;potom ešte odborná literatúra, kde som kupoval knihy o&#160;kreslení, 3D grafike a spracovaní skla. Veľmi rýchlo som si uvedomil, že ma vlastne tie obory ani až tak nezaujímajú, tak tie knihy som len prelistoval, či len urobil pokusy o&#160;prečítanie. Utvárali moje zameranie v&#160;živote práve tým, že som ich nechcel čítať.

A tým sa ich úloha v&#160;mojom živote skončila. Je možné, že sa zopár kníh do môjho života niekedy znovu vráti. Je mrhanie peniazmi či materiálom sa potom tých kníh zbavovať? Ja myslím, že nie. Často až keď niečo odíde z&#160;nášho života, si uvedomíme, ako nám na tom záleží. Alebo nezáleží. Radšej sa navždy či načas niečoho zbavím, než byť z&#160;toho otrávený do konca života.

Čo sa týka zoznamu kníh na prečítanie, zvykol byť dlhý, ale už nie je. Nemá zmysel sa stresovať tým, že som neprečítal všetko, čo som chcel. Väčšina zoznamu zmizla vtedy, keď som aktívne začal zháňať knihy, čo som chcel prečítať. No, vlastne nechcel. Aj teraz sa mi občas stane, že si v&#160;knižnici požičiam knihy, ktoré potom vrátim bez jej otvorenia. No čo, stáva sa.

S&#160;technickou literatúrou je to u&#160;mňa asi najťažšie. Bolo mnoho kníh na internete, ktoré som chcel prečítať. Roky som ich mal posťahované či v&#160;záložkách. Ale roky som sa k&#160;nim neprepracoval. Nebolo treba, a&#160;keď som ich otvoril, bol som z&#160;toho znudený a otrávený. Jedného dňa po obdobiach frustrácie som ich zmazal a zmazal zo záložiek. Fungujem tak, že ak si na knihu spomeniem, prežila v&#160;mojej realite a môžem si ju prečítať. Čo sa stratilo a ani si nespomeniem čo, tak ma netrápi. Spolieham sa skôr na svoju schopnosť sa rýchlo učiť a vyberať vhodné knihy a materiály, keď treba a cítim sa na to.

Niekedy sú knihy ako [Mythical Man Month](https://en.wikipedia.org/wiki/The_Mythical_Man-Month) od Brooksa zúfalo zaujímavé, ale musel som sa prinútiť ich čítať. Dal som si na čítanie obmedzenie tri dni, a&#160;potom som mal myseľ nastavenú na to, že to skončí tak či onak. A prečítal som prvú tretinu knihy, čo bolo tak akurát. Ďalej som už nemal nutkanie čítať. Ako nemôžem si to odškrtnúť ako prečítané v&#160;tradičnom zmysle to-do zoznamov, ale pre mňa je to skončené.

Čo sa týka mojej súkromnej knižnice, tak som všetky knihy či mangu predal, daroval do knižnice, sestre či dal na rozobratie do tej knižnice na ulici. Boli tam cenné knihy, ale už som bol tak ubitý knihami, že som sa ich zbavil rýchlo. Na ničom inom mi vtedy nezáležalo. A bolo to dobré rozhodnutie. Stále mám nejaké knihy a za posledný rok som kúpil asi dve. Dokopy mám 4 mangy a jednu knihu o&#160;karate. Jednu knihu o&#160;Nitrianskom kniežatstve požičanú od sestry. Zvyšok si požičiavam. Prepáč [Level](http://www.level.cz), aj teba som si prestal kupovať. Kúpil by som si ho digitálne, keby bol v&#160;CBZ alebo PDF formáte bez DRM ochrany, ale to je už na iný článok. Tak ho čítam v&#160;knižnici.

Zbavenie sa kníh je tak super. Vonku je toľko ďalších kníh a znalostí, ktoré nás čakajú. V&#160;súčasnosti je prebytok informácií a povaľujú sa všade možne. Treba to využívať a hýbať sa rýchlo. Správna kniha by nás mala pohltiť a mali by sme pri jej čítaní cítiť zmysel.

Ako sa vraví, _znalosti sú sila._ Ale knihy sú len prostriedkom k&#160;dosiahnutiu cieľov. A vždy som hladný po nových znalostiach.
