---
title:  Předplatné časopisu, které nečtete? Prostě ho zrušte
author: Michal
---
A tímto titulkem bych mohl tento minimalistický zápisek v&#160;zásadě i uzavřít, ale nedá mi to, abych k&#160;tomu přeci jen nenapsal i pár řádek. :·)

Tohle téma mi je celkem blízké&#160;– mívával jsem totiž jeden časopis, který jsem rád četl, přišel mi zajímavý, a měl jsem ho proto předplacený (konkrétně to byl Forbes, ale nemůžu ho doporučit).

V&#160;jeden okamžik jsem ho přestal stíhat přečíst celý v&#160;daném měsíci a než jsem se nadál, tak jsem byl celá tři čísla pozadu. Nepřišlo mi to jako zásadní problém, tak jsem si předplatné bez starostí prodloužil. Zanedlouho jsem se ale těmi hromadícísemi čísly začal cítit zavalený a možná je četl i méně než předtím.

Namísto toho, abych tedy předplatné zrušil, jsem si namlouval, že si najdu čas, abych to všechno dočetl. To se v&#160;podstatě dodnes nestalo.

Předplatné jsem sice nakonec zrušil, ale pozdě. To mělo přijít nejpozději v&#160;momentě, kdy mi přestalo život obohacovat, ale převážilo jeho zatěžování.
