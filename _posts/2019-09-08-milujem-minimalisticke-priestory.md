---
title:  Milujem minimalistické priestory
author: Ivan
---
Milujem, keď vôjdem po dlhom dni do svojej izby a cítim veľa priestoru. Môžem vypnúť a uvoľniť sa.

Aby ste pochopili, prečo si to tak veľmi vážim, napíšem, ako som vyrastal a čo som videl. Väčšinu detstva som trávil v&#160;paneláku a vonku. Teraz som nadpriemerne vysoký, takže dosť priestorov mi príde stiesnených. V&#160;detstve, s&#160;malou výškou mi aj byt prišiel ako preliezka a poskytoval kopu miest, kde sa dalo skryť.

Už vtedy ma ale trápilo to, že mám v&#160;detskej izbe málo miesta. Nešlo o&#160;to, že sme tam bývali dvaja alebo traja. Keď veci majú zmysel, všetko ide. Ale zároveň sme v&#160;izbe mali aj dve či tri skrine, kde boli veci, ktoré sa za celé moje detstvo nepoužili. Prevažne látky, oblečenie a skriptá zo strednej a vysokej školy mojich rodičov. V&#160;obývačke sme mali vitríny s&#160;pohármi, ktoré sa snáď nikdy nepoužili. Že pre hostí.

Nejak som to prežil, mal som obdobia, kedy som na to poukazoval, ale keď som mal pätnásť rokov, tak som si povedal, že keď som ignorovaný, tak sa uzavriem a prežijem tie tri roky a už sa nikdy nevrátim. A budem mať svoj veľký priestor podľa seba.

Za detstva sa vždy dalo ísť von, na tréningy, ku kamarátovi, na chatu. Teraz, keď pracujem, tak je to iné. Trávim dosť času vnútri a doma.

V&#160;našej rodine je zvyk mať veľa vecí v&#160;bytoch normálny. Ale jedna moja teta to doviedla až do krajnosti a stala sa zberačkou. Zapratala celý svoj dvojizbový byt veľký 65 metrov štvorcových tak, že tam boli [len cestičky](/2019/03/doma-jen-cesticky-miti/). Všade po stranách boli tak meter a pol vysoké kopy… vecí. Niečo historické, väčšina letáky, krabice, fľaše, textil. Ak si pamätáte tie predajné akcie s&#160;lacnými vecami, kde sa hrnuli dôchodci vo veľkom, tak takto tie veci skončili.

Ale ani v&#160;dospelosti som si hneď nevytvoril minimalistický priestor. Nemal som ešte to nastavenie mysle. A tiež som bol nemajetný študent, takže som býval s&#160;ďalšími tromi ľuďmi na izbe.

O&#160;pár rokov som viac menej mal minimalistickú izbu, ale vtedy som si to ešte neuvedomoval a nedocenil. Stále som nemal minimalistiský život a mentálny stav, ba práve naopak, najviac všetkého.

Bolo toho ešte ďaleko viac a to všetko ma formovalo.

Ale aby som zhrnul svoje túžby na priestor:

- základ: môžem si ľahnúť na zem na anjelika,
- ideálne vysoké stropy,
- alebo aby tam nebol luster, do ktorého by som sa trieskal hlavou,
- nebol tam neužitočný nábytok (merítko sa s&#160;časom mení),
- keď som vošiel do izby, tak mal okamžite pocit priestoru a nie stiesnenosti,
- biele steny, resp. nie bledo hocijakej farby.

Odkedy som sa presťahoval, z&#160;nábytku mám len posteľ a skriňu vo veľkej izbe (25 štvorcových metrov) a [to mi zatiaľ stačí](/2019/03/hlavne-to-neprehanet/). Zistil som, ze nepotrebujem počítačový/písací stôl (zatiaľ), kozmetický stolík, kde som odkladal riad&#160;– dá sa aj na zem, a ani sekretár&#160;– už nemám [žiadne knihy](/2019/03/ivanova-minimalisticka-kniznica/) ani kultúru. Na rôzne veci mám takú krabicu a debničku. Tiež oblečenia mám menej. Pošli [zberateľské veci](/2019/04/jedinecne-veci/). A tiež som odstránil plagáty z&#160;rovnakého dôvodu ako knihy&#160;– posunul som sa ďalej.

Teraz som úplne spokojný, aj keď to ostatní považujú za nezvyklé. Ale tak v&#160;starom Ríme sa nábytok prenášal z&#160;jednej izby do druhej a v&#160;súčasnosti tiež nábytok nie je v&#160;zemiach tretieho sveta bežný. Takže sa dá prežiť aj bez neho. Asi je to odrazom mojej psychiky, že nemám potrebu si pomeriavať hodnotu osoby množstvom nábytku a vecí. Spokojnosť v živote je pre mňa to najdôležitejšie.
