---
title:  Nehromaďte malé úkoly
author: Michal
---
„Tímhle se teď nebudu zdržovat, to sfouknu jindy.“

Nevím jak u vás, ale u&#160;mě docela častá myšlenka. Ona dává smysl. Pokud jsem uprostřed něčeho, je efektivnější jiné věci odložit a nerozptylovat se (nebo, jak říkají profesionálové v&#160;tomto oboru, neprokrastinovat).

Stejně tak je ale důležité i to, aby tyto malé věci k&#160;udělání nezůstaly neudělané (příliš dlouho). Nevyhnutelně se začnou připomínat, vracet se na mysl a kromě rozptylování s&#160;nimi přijde i pocit nezvládání. Příliš dlouho odkládaná věc může nahlodat psychickou pohodu a člověk se může cítit zavalený (případně zavalenější než předtím).

Naopak, vyhradit si někdy tu chvíli k&#160;udělání toho kterého miniúkolu přinese psychickou úlevu a dobrý pocit, že je to hotové a pryč. A že člověk má věci přeci jen pod kontrolou a není vprostřed chaosu.
