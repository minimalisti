---
title:  Novoroční cetky
author: Michal
image:  novorocni_cetky-cb.jpeg
image-alt: Hromádka novoročních cetek od jedné firmy
---
Naštěstí už se to moc nevidí, ale i tak se můžeme setkat s&#160;firmami, které své klienty zcela automaticky zaplavují novoročními balíčky plných cetek. Při pohledu na takovou zásilku vstávají minimalistovi vlasy na hlavě.

Dovolil jsem si udělat anonymní fotku, co jedna společnost zaslala tento rok mně. V&#160;obálce byla papírová taštička, v&#160;ní PFko (soudě podle trhu nejspíš papírové, byť na pohled vypadá plastově), fotku pracovního kolektivu, bloček, propisku a cestou rozdrcený perníček. Předešlý rok poslali propisku, svítilničku a přívěšek na klíče. Vždyť to ani není reprezentativní!

<figure class="post-image">
  <img src="/images/posts/novorocni_cetky.jpeg" alt="Hromádka novoročních cetek od jedné firmy">
</figure>

Kdo by o&#160;takové cetky stál? Je to k&#160;ničemu, akorát odpad (no dobře, propiska se vypíše, ale copak člověk nemá čím psát, že mu musí někdo vnucovat svou propisku?).

Skutečně mě to naštvalo. Tedy pravý opak toho, co ta firma zásilkou zamýšlela. Do diáře jsem si na začátek následujícího prosince zanesl připomínku, abych firmě připomenul, že o&#160;tento typ „pozornosti“ nemám zájem. Není potřeba být nějak nepříjemný&#160;– lze jednoduše poděkovat a odkázat se například na šetrný přístup k&#160;životnímu prostředí. Řešení není nutně složité.
