---
title:  Pasivní mentální zátěž
author: Michal
image:  pasivni_mentalni_zatez.jpeg
image-alt: Mozek s košem plným závazků na zádech
---
Je to něco, co člověk nevnímá, neví, že to existuje, ale uvědomí si to, jakmile o to začne přicházet. Což je v&#160;případě pasivní mentální zátěže samozřejmě pozitivní. Je to jeden z&#160;těch dobrých pocitů, které člověku minimalismus přináší.

Pasivní mentální zátěž způsobuje už pouhopouhé vlastnění (příliš mnoha) věcí. Tím, že něco mám, dávám nevědomky dané věci slib, že mi k&#160;něčemu bude, že ji budu používat. Pokud ale takových slibů rozdám příliš mnoho, budu se jimi vnitřně cítit přetížen&#160;– a&#160;to i&#160;když si to neuvědomuji a nejsem ani schopen ten pocit pojmenovat.

Když něco přestanu používat nebo potřebovat a uložím to například do sklepa, dělám to z&#160;nějakého důvodu. Typicky „ještě se mi to může hodit“ nebo „je to pěkné, třeba se k tomu někdy vrátím“. V&#160;pokročilých stádiích mi ještě bude vadit, že nemám žádný prostor, žádné volné místo&#160;– protože už je zaplněné věcmi. Které nepoužívám. Ani nepotřebuji. Ale co kdyby!

<figure class="post-image">
  <img src="/images/posts/pasivni_mentalni_zatez.jpeg" alt="Mozek s košem plným závazků na zádech">
</figure>

Nevěděl jsem, že to přijde, ale když jsem se zbavil citelného množství takovýchto věcí, cítil jsem úlevu. Najednou mou mysl opustilo stejné množství imaginárních závazků vůči těmhle věcem, které jsem už neměl. Dobrý pocit. Naráz jsem tomu uměl dát jméno.

Přesně tahle redukce pasivní mentální zátěže je jednou z&#160;mých motivací, proč chci žít „minimalisticky“.
