---
title:  Odsouzeno ke smazání za 3… 2… 1…
author: Michal
---
Znáte to. Stahujete nebo vytváříte nějaký soubor, který skladovat nechcete, ale nějakou dobu ho budete potřebovat. Tak ho prostě uložíte na disk (dost často jen tak do domovského adresáře nebo dokumentů)&#160;– s&#160;myšlenkou, že až ho přestanete potřebovat, tak ho samozřejmě smažete.

Mým častým problémem je, že takových souborů mám desítky, případně stovky. V&#160;okamžiku, kdy bych už mohl daný soubor smazat, bych musel projít to kvantum souborů a rozhodnout, které jsou ty, které už nepotřebuji.

Ve výsledku tenhle proces prakticky neprovádím a všechno se mi hromadí. Došel jsem ale k&#160;překvapivě funkčnímu řešení: k&#160;expiračním složkám. Názvem složky je datum, kdy složka expiruje&#160;– tzn. po tomto datu ji smažu.

Na začátku si tak rovnou rozhodnu, kdy budu chtít nějaký soubor (pravděpodobně) smazat, a podle toho ho zařadím do příslušné složky. Před mazáním můžu ještě překontrolovat, že už skutečně nic z&#160;toho, co ve složce je, nebudu potřebovat, ale už nemusím identifikovat kandidáty ke smazání a rozhodovat se. Rozhodnuto už totiž mám.

Stejný přístup je velice jednoduché aplikovat i na papírové dokumenty a jiné fyzické věci. Dohromady se mi tak daří podstatně lépe zvládat nezbytné skladování s&#160;docela malou energetickou/časovou investicí.
