---
title:  Nejde-li to za peníze, třeba to půjde zadarmo
author: Michal
---
Mám [relativně bohatou nabídku věci](https://www.sbazar.cz/m.zima) (více než 100 inzerátů), které bych rád poslal dál někomu, kdo je využije, když já pro ně využití nemám a skladovat je také nechci. Ale některé věci se neúspěšně snažím udat již dlouhé roky. A popravdě mi už s&#160;nimi začíná docházet trpělivost…

Ta [zátěž](/2019/01/pasivni-mentalni-zatez/) neužitečného vlastnění se mi časem kumuluje a začínám si říkat, jaký to má vlastně smysl. **Já ty věci nechci.**

V&#160;inzerátech jsem je nabízel v&#160;podstatě za pár korun. Je sice příjemné něco za něco dostat, ale vždyť to pro mě ve skutečnosti neudělá žádný finanční rozdíl, jestli věc prodám za 20 korun nebo vyhodím. Tak jsem dal pár desítek věcí [za odnos](/2019/05/neber-veci-zadarmo/).

Je pravda, že jsem čekal větší fofr, ale nějakých 30 věcí jsem už během prvních týdnů udal a je to fyzicky vidět! Asi nejzajímavější je ale to, že o&#160;některé věci se hlásí i zájemci, kteří se pro ně stavit nemohou a nabízí mi peníze za zaslání poštou. A zpravidla více, než za kolik jsem je dlouhou dobu předtím nabízel. Případně se staví, ale koupí si k&#160;tomu i věci, které za odnos nenabízím. S takovým efektem jsem sice nepočítal, ale je příjemný a dává smysl.

Z&#160;toho, že tolik věcí najednou konečně nemám, mám značnou radost. Velice dobrý pocit mám i z&#160;toho, že ty věci najdou smysluplné využití namísto toho, aby mi dál planě ležely někde uložené nebo abych je naprosto bez užitku vyhodil (nesnáším vyhazování funkčních a použitelných věcí!).
