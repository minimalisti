---
title:  Cieľ minimalizmu – byť sa sám sebou
author: Ivan
---
Jedným z&#160;aspektov minimalizmu je ten, že sa stávame samými sebou. Priznám sa, že neviem ako *Marie Kondo* nechať si iba tie veci, ktoré jej spôsobujú radosť a dokončiť celý proces za pár dní alebo týždňov. Myslím, že niečo také vyžaduje duševne vyspelého človeka, čo vie, čo chce.

Ja už teraz v&#160;roku 2020 tiež môžem viac menej povedať, že viem, čo chcem, ale stále sú tu temné a sivé skupiny vecí, ktoré mám, aj keď ich nechcem, mám z&#160;nich doslova zmiešané pocity alebo absolútne netuším, čo s&#160;nimi. V&#160;istom zmysle to odráža môj duševný stav a vyspelosť.

Pri mnohých veciach potrebujem týždne až mesiac na rozlúčenie sa a premyslenie si, čo to pre mňa znamená. Niektoré veci som skúsil predať, ale uvedomil som si, že si ich chcem nechať, ale zároveň ich denno denne používať, a nie si ich „šetriť“. Celý ten proces ide veľmi pomaly, ale teraz už môžem povedať, že som spokojný s&#160;tým, ako to vyzerá. Vrásky na čele mi spôsobuje už iba menej než 10 vecí.

Budem potom sám sebou? Verím, že áno, ale zároveň sa stále mením, takže tanec nikdy neskončí.
