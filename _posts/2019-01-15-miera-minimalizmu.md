---
title: Miera minimalizmu
author: Ivan
---
Koľko musí mať človek vecí, aby sa mohol považovať za minimalistu?

Dajme si pár príkladov.
Najmenej vecí majú asi digitálni nomádi, napríklad [Levels](https://levels.io/minimalist-wardrobe/) má toho celkom málo.
Tiež sú tu projekty ako vydržať sa obliekať z&#160;menej ako [33 vecami 3 mesiace](https://bemorewithless.com/project-333/).

Uvedomil som si, že ide hlavne o&#160;okolnosti.
Ako digitálny nomád sa spolieham, že mám poruke
kreditku plnú peňazí, chodím po tropických krajinách, kde nie je zima a môžem si kúpiť alebo prenajať veci ako bicykel
a služby ako strihanie vlasov a pranie a neriešim veci ako nástroje na údržbu, bytové doplnky a posteľ.

**Najdôležitejšia vec na ceste k&#160;minimalizmu je nechať si iba tie veci, čo prinášajú radosť.** Pôvodne som si myslel, že mám relatívne málo vecí
a že ak sa zbavím kníh a nejakého oblečenia, tak bude hotovo. Horkýže! Bolo toho ďaleko viac a boli to také cykly. Zbavil som sa kníh a hľa,
objavili sa nejaké ďalšie staré časopisy a vysokoškolské skriptá. Ale po istom čase, nejakých 10 cykloch, som si uvedomil,
že sa cítim hlbšie šťastnejšie a slobodnejšie a že vlastne veci mám rád.

Čo mám? 80l kufor oblečenia. Nejaké bundy, dva svetre a zimné veci. Mám nejakú výbavu do prírody ako nohavice M65, stan, spacák.
Strojček na vlasy, nožnice na vlasy. Tiež matrac, kôš na prádlo a sušiak. Pomerne spústa malých vecí, čo potrebujem.

Avšak čo už nemám oproti minulému roku? Knihy a filmy. Nechal som si iba menej ako 4 kusy z&#160;každého. Oblečenie&#160;– pár vecí som presunul ako pracovné k&#160;rodičom a veci, čo som vyslovene nechcel nosiť, tak šli do kontajnerov na textil.
Dal som preč nejaké kuchynské veci a nechýbajú mi,
lebo sa objavili nejaké iné, spoločné v&#160;byte. A preč šiel PlayStation 4, hry, dva počítače a jeden záložný mobil. A nemám 99&#160;% digitálnych súborov. Pošla pošta, záložky, hudba, stará vlastná tvorba, písma, zdrojové kódy mojich aj cudzích programov, fotky. Digitálna očista už začala
roky dozadu, prvotné začiatky „neviem, čo robím“ už 10 rokov dozadu. Nakoniec sa ukazuje, že s&#160;fyzickými vecami to je podobne.

Jednoducho to nemalo zmysel a budúcnosť. Skrátka nechal som si veci, čo mi pomáhajú v&#160;súčasnosti a tie z&#160;minulosti musia ísť preč.
Som im vďačný, ale ak by som si ich nechal, spôsobilo by to len zlú krv.

Považujem sa za minimalistu? Rozhodne. Keď sa rozhliadnem okolo seba, mám máličko vecí. Koľko? Hmm, okolo 500 litrov.
