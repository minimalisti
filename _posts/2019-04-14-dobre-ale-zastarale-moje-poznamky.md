---
title:  Dobré, ale zastaralé. Moje poznámky
author: Michal
image:  obalky-cb.jpeg
image-alt: Můj organizér obálek s poznámkami
---
Miluji papír. Je to věc, která nemá konkurenci, a velké množství věcí jednoduše zapisuji na papír. Včetně všech svých zápisků na Minimalisty. Takto jsem si kdysi vybudoval celý systém organizace poznámek, které jsem zachycoval na větší i menší papíry.

Základem systému je členění poznámek do oblastí či přímo konkrétních projektů. Každá oblast nebo projekt má vyhrazenu svou obálku (mými oblíbenými jsou modré volební obálky A5, které takhle recykluji). Protože obálek je hodně, vyrobil jsem si na ně z&#160;kartonu ze staré krabice vlastní pořadač. Krásně se mi vejde na stůl a kdysi se mi do něj pohodlně vešly všechny obálky.

Kdysi. Pak už jich ale začalo být příliš mnoho, takže jsem některé musel skladovat jinde.

Už nějaký ten rok nejsem se svým systémem úplně spokojený. Má totiž hned několik problémů. V&#160;prvé řadě u&#160;mnoha obálek nevím, co v&#160;nich mám. Těch poznámek je příliš mnoho nejen na udržení si přehledu, ale i na samotnou práci s&#160;nimi (a&#160;to nemluvím o&#160;nepraktičnosti probírání se hromádkou malinkatých papírků). Dalším bolavým místem je to, že uchovávám poznámky i k&#160;mnoha starým projektům a záležitostem, které už jednoduše nejsou (a ani už nikdy nebudou) aktuální a uplatnitelné.

Přestože všechno tohle vím už dlouho, nebyl jsem schopný to vyřešit&#160;– klidně jsem prošel celou obálku a zase ji vrátil zpátky. V&#160;posledních dvou třech týdnech jsem se rozhodl do toho pustit znovu&#160;– tentokrát však posílen minimalistickým přístupem a odhodláním zbavit se všeho, co pro mě už nemá hodnotu či co se týká věcí, které vnitřně vlastně vůbec nechci dělat.

Zvolil jsem postupný a iterativní přístup&#160;– zpracovávám vždy jednu obálku po druhé a snažím se objektivně zhodnocovat jejich obsah. Osvědčilo se mi udělat s&#160;odstupem pár dnů ještě další iteraci nad tím, co jsem se napoprvé rozhodl si zatím nechat, a zredukoval jsem to ještě dále.

<figure class="post-image">
  <img src="/images/posts/obalky.jpeg" alt="Můj organizér obálek s poznámkami">
  <figcaption>Než jsem začal, tak tahle věc byla kompletně napěchovaná</figcaption>
</figure>

Na fotce je vidět aktuální stav&#160;– prvních asi pět obálek je již zcela prázdných (a vyhodím je), další už jsou také z&#160;části probrané. Výslednou kupu papírů (ač se to nezdá) trvalo skartovat asi patnáct minut. A&#160;to jsem teprve začal.

Smyslem poznámek je, aby byly užitečné a používaly se, takže jednou z&#160;dalších fází bude i (částečná?) elektronizace a začlenění do vhodné struktury v&#160;[zimu](https://www.zim-wiki.org/).

Z&#160;dosavadních výsledků mám radost a těším se na pokračování. Je to skutečně jako [zbavovat se staré zátěže](/2019/01/pasivni-mentalni-zatez/). Vyhozením poznámek se vzdávám jakéhosi závazku na daném projektu (ještě) někdy pracovat, čímž se mi otevírá více prostoru pro věci/projekty, které skutečně dělat chci.
