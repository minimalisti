---
title:  Zásob pro tři generace
author: Michal
image:  minituzky-cb.jpeg
image-alt: Krabička plná minitužek
---
Začíná to nenápadně: člověk typicky předvídá nějakou budoucí (s)potřebu. A jelikož neví, kolik přesně bude potřebovat, koupí toho raději trochu víc. Třeba školní potřeby, když začínají děti chodit do školy&#160;– aby sešitů, tužek, obalů atd. bylo dost. Navíc v&#160;září jsou vždy za lepší ceny, tak není důvod neudělat zásoby.

A zatímco některé věci se skutečně spotřebují, jiné zůstanou netknutě ležet. A teprve až ona potřeba opadne, člověk uvidí, co vlastně nepotřeboval. Ale což, ono se to někdy ještě hodí. Ledaže by ne.

Jindy člověk jednoduše miluje určité věci a kdykoli na ně natrefí, tak nějaké pořídí. Přestože jeho spotřeba je podstatně nižší, než kolik jich (už) má.

Tento scénář se týká i tužek na fotce. Jeden člověk je s&#160;oblibou shromažďoval&#160;– až si jednoho dne uvědomil, že tužkami vlastně nepíše a že je nepotřebuje. Tak mi je dal…

<figure class="post-image">
  <img src="/images/posts/minituzky.jpeg" alt="Krabička plná minitužek">
  <figcaption>91 minitužek (kolik jich tak člověk spotřebuje za rok, že?)</figcaption>
</figure>

A nejsou to jen tužky, kterých mám nadměrnou zásobu. Potíž je, když má člověk ve zvyku lepší věci „šetřit“. Ale pokud se něco nepoužívá, jaký má smysl to vůbec mít?

Takže jsem trochu přeoral svoji spotřebu. Využívám, co mám, kdykoli se pro to naskytne příležitost. Ani ne tak kvůli tomu, abych to nutně spotřeboval, ale aby ta věc dávala smysl. Proto ji přeci mám. A musím říct, že tato drobná změna v&#160;přístupu byla ve skutečnosti významným posunem k&#160;lepšímu.
