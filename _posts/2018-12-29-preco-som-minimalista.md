---
title: Prečo som minimalista?
author: Ivan
---
Minimalizmus je pre mňa predovšetkým otázkou osobnej slobody a posunu v&#160;živote. Je o&#160;urezávaní častí života a vecí,
ku ktorým už nemám žiadny citový vzťah alebo ma dokonca desí, že by som sa k&#160;nim vrátil.

Ako som starší, začal som uprednostňovať viac fyzický a pomalší život. Stále mám možnosť do zblbnutia hrať veľa počítačových hier,
pozerať nekonečno seriálov a filmov, počúvať nekonečno hudby... ale to všetko je zbytočné, ak si to nedokážem náležite vychutnať.

A čo sa týka vecí&#160;– začiatkom roku 2018 som si uvedomil, že som si postavil z&#160;vecí obrovské väzenie a už netúžim po tom, aby to išlo ďalej.
A to som sa snažil upratovať veci už roky, ale myslím, že ma posunula až kniha od [Marie Kondo](https://konmari.com/pages/about) *The Life-Changing Magic of Tidying Up*,
ku ktorej som sa dostal v&#160;2017.

Ale až tento rok som sa pevne rozhodol rozlúčiť s&#160;vecami nielen fyzicky, lebo by sa ku mne aj tak vrátili späť
alebo by som kúpil niečo podobné,
ale aj mentálne. Poďakoval som im a rozlúčil sa s&#160;nimi ako to radila Marie Kondo. Najprv som nevedel, že výsledok bude taký dobrý. Zo začiatku som to urobil skôr
preto, že nič lepšie som nevedel.

Ale o&#160;pár mesiacov neskôr som pochopil, že v&#160;živote sa musíme vecí okrem získavania aj zbavovať. Je zo začiatku desivé pomyslieť si, že bez veci
X nemôžem robiť vec Y. Ale nakoniec na tom nezáleží. Koľkokrát som si vravel, že si ešte raz prečítam tú knihu, zahrám tú hru alebo budem
posilovať s&#160;tými činkami. A nikdy som to neurobil. Možno iba trochu, aby sa nepovedalo. Vlastne, tie veci ma po čase utláčali, lebo som cítil vinu, že ich náležite nevyužívam.

Preto som ich posunul ďalej&#160;– niečo predal, mnoho daroval a časť vyhodil. A cítim sa šťastnejšie. Zopár z&#160;vecí ako gitara a bicykel,
nakoniec pri mne zostali aj keď pôvodne som ich neváhal predať. A teším sa z&#160;nich. Konečne...
