---
title:  Méně odpadu, méně starostí
author: Michal
---
Změnil jsem se. Má krabice na plastový odpad je toho svědkem. To je totiž ta věc, která nejde přehlédnout.

Ale pojďme od konce. Tohle je všechen můj (poctivě tříděný) plastový odpad za celý rok:

<figure class="post-image">
  <img src="/images/posts/michaluv_rocni_plastovy_odpad.jpeg" alt="Malá (na výšku ~27cm) igelitová taška napěchovaná z ~85 % plastovým odpadem">
</figure>

Můj život nečekaně hodně ovlivnil [dokumentární film o albatrosech laysanských](https://www.albatrossthefilm.com/) (doporučuji). Albatrosi žijí na moři a hnízdí převážně na ostrově Midway, který je co do vzdálenosti od kontinentu nejodlehlejším místem na Zemi. Přesto na ně, na jejich mláďata má náš dnešní způsob života dramatický vliv.

Já nemám plasty rád a už dříve jsem se jim snažil vyhýbat, ale tento film mi otevřel oči.

Jednoduše koukám na to, co nakupuji a jak nakupuji. V obchodech si neberu mikroténové sáčky, mám vlastní, mám plátěné, mám bavlněné síťky, nekupuji nápoje v plastech ani tetrapacích, mléko beru ve vratném skle, nakupuji v bezobalových prodejnách do vlastních nádob, nejím průmyslově zpracované potraviny, [hledám alternativy k plastům](https://www.mujmalysvet.cz/eko/nahrady/).

Celé to není nějak složité, a navíc tak žiji zdravěji a jím chutněji. Ale je to změna. Chce to chtít. Ve výsledku mám i méně starostí – vlastně vůbec nechodím s plasty, nesmrdí mi přitom doma a ani nenadávám, že už zase přetéká krabice na ně. Jednodušší život.
