---
title:  Doma jen cestičky míti
author: Michal
---
Až groteskně jeviti může se představa, že členové někteří společnosti naší mají domovy své zaplněny do míry té, že prodírati se jimi musí cestičkami úzkými jako zvíře norou svou. Mýtem nejsou však! Skutečně existují.

Není mi ještě známo zcela, jak se do takovéhoto stavu dopracovati lze, domnívám se však, že postupně. Zatím nikdo se mi přímo nesvěřil, že osobně on by život takový vedl, však o příbuzných svých několik osob vypovědělo již. Fascinující to příběhy.

Předchozí zřízení socialistické s výrobou centrálně plánovanou nebylo kvalitou výrobků svých zrovinka proslulé. Jeden muž se tak shodou okolností sběratelem stal. Všemožných to spotřebičů. V bytě jeho skvělo se tak kupříkladu televizorů šest prakticky nových. Žel přírodě žádný z nich provozu schopný nebyl. Možná zamýšlel je původně opraviti – nikdá se však k tomu činu nedopracoval. Stejnému osudu podlehlo tak i ostatní bohatství království jeho.

To však ještě nic není proti ženě jedné vysoce organizované a pořádné! Domácnosti její striktní systém vládl. Vše místo své pevné mělo, vše řádně označeno a popsáno bylo. Polámané do vlasů spony? „Slepit,“ skvělo se na krabičce úhledné v jedné z přihrádek nábytku jejího. „Noviny obsahují informace,“ znělo hrdě moto její – žádné proto opustit byt ten nesměly. Sic nečteny, bez užitku ženě nebyly však! Poctivě do sloupů vyskládány kompenzovaly stabilitu vratkou její a při pohybu po bytě pomáhaly jí tak.

Význam praček několika (ano, čtenáři milí, hádáte správně: z nich každá byla vadou nějakou taktéž obdařena) však všem skryt zůstal. Ani tašky nákupní s kolečky polámanými se šance své nové rovněž nikdy nedočkaly.

Praví legenda, že opustily-li věci její systém uspořádání perfektního (ať už uvnitř skříní či vně), objem svůj několikanásobně zvětšily a zpět se již nevešly.

Proto chraňte se maximalismu a hromadění chorobného, neb stihne vás osud ponurý! Minimalismus, jen minimalismus řešení zná a tou správnou cestou jest. Jen ty věci míti, které účel zřejmý plní a hodnotu do životů našich vnášejí.
