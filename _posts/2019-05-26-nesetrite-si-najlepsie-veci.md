---
title: Nešetrite si najlepšie veci
author: Ivan
---
Jednou z&#160;vecí, čo pozorujem na sebe, ale aj na ostatných je tendencia si šetriť tie najlepšie veci na neskôr.

Oblečenie, ruksaky, poháre, elixíry v&#160;RPG hrách, ale ba aj knihy.

Ak k&#160;tomu nie je dobrý dôvod, nemá zmysel si to kdesi sysliť. Zoberme si napríklad oblečenie. Načo chodiť po dome v&#160;starom oblečení alebo takom, čo sa mi nepáči, keď mi vzadu v&#160;skrini leží to super tričko, čo radšej nikdy nenosím, aby sa mu niečo nestalo?

Nikdy sa to nevypotrebuje. Oblečenie je teraz lacné a zničiť jednu vec trvá tak 5 rokov. V&#160;prípade veľkého šatníka to bude trvať veky. Len sa zbytočne človek obmedzuje a úplne pre nič.

Napríklad Michal si šetril v&#160;skrini svoje obľúbené tričko s&#160;motívom [Big Buck Bunny](https://www.youtube.com/watch?v=aqz-KE-bpKQ), až sa materiál rozpadol a už ho aj tak nemôže nosiť.

Pri jedle som si to tiež uvedomil, keď zúfalo sa snažím zjesť jedlo tesne pred alebo po dátume spotreby a zároveň mám ešte niečo čerstvé. Obzvlášť pečivo býva takéto. Je ho veľa a rýchlo sa kazí. V&#160;mestskom prostredí je to niekedy lepšie vyhodiť a nabudúce kúpiť menej.

Obecne sa mi nepáči, ak nejakú vec mám len „vo vitríne“ a nikdy ju nepoužijem. Lebo je príliš dokonalá alebo to najlepšie, čo mám.

Áno, veci sa opotrebujú, a dokonca aj doslúžia, a bude treba zohnať novú alebo niečo iné. V&#160;tom je tá krása&#160;– nič nie je naveky a tak to môžem bez strachu používať. Dokonca aj keď sa vec nepoužíva, tak starne až sa rozpadne.

Takže&#160;– treba si užívať veci a ich aj používať.
