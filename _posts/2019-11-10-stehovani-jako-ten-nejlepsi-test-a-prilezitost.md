---
title:  Stěhování jako ten nejlepší test (a příležitost)
author: Michal
---
Za normálních okolností typický neminimalista ve skutečnosti vůbec neví, co všechno má. Když se pak má stěhovat, tak je tím, kolik toho má, velice často překvapený nebo volá „jé, to jsem hledal roky!“

A co udělá? Uloží vše řádně do krabic a přestěhuje. Pokud jste se třeba dvakrát stěhovali, měli jste později zhruba stejně krabic (tašek) věcí, méně nebo více? Pokud méně, je k&#160;čemu gratulovat, pokud ± stejně, jste na tom pořád dobře. Narůstá-li vám ale objem stěhovaných věcí bez dobrého důvodu (např. nové přírůstky do rodiny), jste na tom sice stejně jako velké množství dalších lidí, ale přirozeně to v&#160;pořádku není.

Stěhování je jedinečný okamžik, kdy člověk vezme do ruky vše, co má. Toho je přeci třeba využít! Začněte balit s&#160;velkým předstihem, abyste nebyli v&#160;časové tísni. A vždy, než nějakou věc uložíte do stěhovací krabice, se zamyslete:

- Kdy jsem ji naposledy držel v&#160;ruce?
- Kdy jsem ji naposledy použil nebo potřeboval?
- Potřebuji ji?
- Využiji ji?
- Proč ji vlastně mám?
- Má pro mě vůbec nějakou hodnotu, obohacuje nějak můj život?
- Chci ji skutečně stěhovat?

Nepopírám, může to být docela zdlouhavý proces. A tím úmornější, čím více věcí člověk má. Výsledek však stojí za to.

Když jsem se naposledy stěhoval já, tímto způsobem jsem si u&#160;mnoha věcí uvědomil, že je vlastně ze srdce nesnáším a za žádnou cenu je stěhovat nechci. Ledasco jsem [prodal](/2019/01/muj-prvni-krok-na-ceste-k-minimalismu/), jiné věci [daroval](/2019/09/nejde-li-to-za-penize-treba-to-pujde-zadarmo/), zbytek vyhodil. A bylo toho HODNĚ.

Stěhování je příležitost do nějaké míry pročistit svůj život. Konec konců měníte domov a budete svůj život organizovat zase trochu jinak. Tak proč nepřehodnotit i své věci, no ne?
