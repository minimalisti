---
title:  Hranice môjho minimalizmu
author: Ivan
---
Nedávno som si všimol, že aj môj minimalizmus dosiahol svoje hranice.

Koketoval som s&#160;myšlienkou ako sa zbaviť ďalších vecí. Ale nakoniec som si uvedomil, že ich naozaj potrebujem k&#160;životu. To ma celkom potešilo, lebo začínam dosahovať vyrovnaný počet vecí vo svojom živote. Marie Kondo to zvládne pri klientoch za týžden či mesiac, mne to trvalo roky, no. :-D

Druhá vec je, že za ten čas mi stále pribúdali veci, takže som sa ich musel priebežne zbavovať. Našťastie v&#160;poslednom čase už ani nie, keďže už vlastne až tak veľa nekupujem a odmietam veci zadarmo a aspoň zhruba viem, čo chcem.

Konečne môžem celkom zvoľniť, lebo mám oveľa menej vecí a je oveľa lahšie sa rozhodnúť, čo potrebujem a čo nie. Takže trávim s&#160;tým oveľa menej času, snáď o&#160;90&#160;%.

Okrem minimalizmu túžim robiť v&#160;živote aj iné veci&#160;– ako behať či venovať sa niečomu tvorivému. Ale stalo sa mi, že som začal prokrastinovať minimalizmom, čo keď som si uvedomil, tak som začal tvoriť. Menší čas strávený správou vecí mi dal slobodu tráviť čas niečím iným. Už ma veci neutlačujú.

A to je fajn.
