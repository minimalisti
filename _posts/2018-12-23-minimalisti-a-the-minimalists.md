---
title:  Minimalisti a The Minimalists
author: Michal
---
Člověk si žije svůj obyčejný život, než zjistí, že pro řadu lidí je neintuitivní&#160;– a&#160;to až tak, že o&#160;podobném životním stylu vznikl i filmový dokument.

Já (Michal) a Ivan se už roky snažíme průběžně osvobozovat své životy od nepotřebných věcí, věcí, které nás neobohacují, nebo dokonce zatěžují.

Že se tomuhle životnímu směru říká *minimalismus*, jsem zjistil až v&#160;okamžiku, kdy mi kamarád ukázal dokument [Minimalism: A Documentary About the Important Things](https://minimalismfilm.com/) (tedy „Minimalismus: dokument o&#160;důležitých věcech“) od dvou lidí, kteří vedou blog [The Minimalists](https://www.theminimalists.com/) (a miniweb [Minimal Maxims](https://minimalmaxims.com/) se zajímavými myšlenkami/citáty). Ivan pak ještě našel jejich [TEDx](https://www.youtube.com/watch?v=GgBpyNsS-jU) [přednášky](https://www.youtube.com/watch?v=w7rewjFNiys) a [rozhovor](https://www.youtube.com/watch?v=WNAy8DAaHsY) v&#160;jednom podcastu.

Šel jsem sice stejným směrem, ale inspirovali mě k&#160;tomu, abych více zpochybňoval současný stav a ptal se na skutečnou hodnotu.

Mnohem později pak způsobili i to, že jeden z&#160;našich rozhovorů s&#160;Ivanem skončil u&#160;„Hele, minimalisti.cz ještě neexistuje.“ Nebýt inspirace u&#160;našich amerických protějšků, dělat osvětu v&#160;našich vodách by nás nenapadlo. :·)
