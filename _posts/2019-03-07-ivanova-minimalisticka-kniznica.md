---
title:  Ivanova minimalistická knižnica
author: Ivan
image:  kniznica-cb.jpeg
image-alt: Debnička s knihami
---
Väčšina z&#160;nás rada číta, mňa nevynímajúc. Prečítal som v&#160;živote mnoho a postupne sa moja knižnica zmenila z&#160;toho, čo som kedysi prečítal a fandil tomu, na to, čo čítam teraz a čo sa mi páči v&#160;súčasnosti.

<figure class="post-image">
  <a href="/images/posts/kniznica.jpeg">
    <img src="/images/posts/kniznica.jpeg" alt="Ivanova minimalistická knižnica">
  </a>
  <figcaption>Tri knihy nie sú moje.</figcaption>
</figure>
