---
title:  Digitálna mentálna záťaž
author: Ivan
---
Digitálny minimalizmus je pre mňa rovnako dôležitý ako fyzický. Ako napísal Michal v&#160;[minulom príspevku](/2019/01/pasivni-mentalni-zatez/), zbytočne strácame psychickú energiu vlastnením nesprávneho množstva fyzických vecí.

Vzhľadom na to, že digitálny svet je skoro nekonečný, je o&#160;to ľahšie mať kopy nepotrebných digitálnych vecí&#160;– súborov. S&#160;tým ako je teraz všetko prepojené, rozsiahle a rýchlo sa meniace, nedáva mi už moc zmysel zhŕňať súbory. Aj uloženie súboru je sľub či kontrakt, že sa mu budem venovať alebo že ho využijem.

Je len málo súborov, ktoré so mnou nakoniec vydržali viac ako päť rokov. Ako dieťa a tínedžer, ktorý nič nevedel o&#160;svete, všetko mi prišlo nové. Tak som toho mal veľa. Ale zhruba po desiatich rokoch prišla prvá vlna, keď som si uvedomil, že už viem dosť vecí. A množstvo súborov sa stalo zrazu irelevantnými. Aj tak som do nich nepozeral už roky.

Tiež som trpel strachom, že sa niečo stratí z&#160;internetu a už to nikdy neuvidím. Bol to oprávnený strach. Dosť vecí sa stratilo. Ale časom som zaujal postoj, že na tom aj tak nezáleží. Príde niečo iné, nové. Tiež som ďaleko viac veril v&#160;svoje schopnosti niečo vytvoriť.

Treťou najväčšou kategóriou boli moje vlastné výtvory. Čím viac som ich ukladal, tým menej som vytváral a experimentoval na nových. Lebo som chcel dokončiť tie staré, aj keď v&#160;hĺbke duše som sa k&#160;nim už nechcel vracať. So zmiešanými pocitmi som ich zmazal a&#160;aj keď stále na ne jasne spomínam, sú preč a ukázalo sa, že je to dobré.

Je cennejšie vytvárať niečo nové, než tráviť čas so súbormi, ktoré nám už nemajú čo dať.
