---
title:  Nejlepší dárek pro minimalistu
author: Michal
image:  prazdny_darek.jpeg
image-alt: Mašle jako kdyby uvázaná na dárku
---
Vymýšlet dárek pro minimalistu může být skutečný oříšek. Obtížně odhadnete, co potřebuje nebo mu jistě udělá radost a co naopak ve skutečnosti vyvolá v&#160;jeho hlavě paniku, co si propána s&#160;takovou věcí počne.

Tyto Vánoce jsem dostal skutečné překvapení. Rozbalil jsem krabici větších rozměrů. Byla zalepená, ale říkala, co bude uvnitř. Okamžitě jsem začal přemýšlet, co s&#160;tím jenom budu dělat. Zkrátka starosti.

Velice mě však překvapilo, když jsem krabici otevřel. Ta věc tam totiř nebyla. Měla být, ale jednoduše nebyla. Jen ochranná výplň.

<figure class="post-image">
  <img src="/images/posts/prazdny_darek.jpeg" alt="Mašle jako kdyby uvázaná na dárku">
</figure>

Překvapilo to dokonce i dárce samotného. Tento originální, nečekaný a neplánovaný dárek se tedy paradoxně stal tím, který mi udělal největší radost. Právě vším tím překvapením, ale i tím, že se trefil&#160;– okamžitě se rozplynuly všechny starosti s&#160;věcí, zůstala jen radost z&#160;prázdné krabice.

Musím ale varovat, že prázdná krabice rozhodně není univerzální&#160;– každý obdarovaný si ji může vyložit jinak. Nedovedu vlastně ani já říct, co bych na ni říkal, kdyby byla plánovaná.
