---
title:  Schraňování starých časopisů
author: Michal
image:  hromadka_casopisu.jpeg
image-alt: Hromádka časopisů
---
Svého času jsem odebíral předplatné i dvou tří časopisů měsíčně. A bylo to perfektní. Měl jsem ty časopisy rád, ohromně mě obohacovaly, a měly tak pro mě velký význam.

<figure class="post-image">
  <img src="/images/posts/hromadka_casopisu.jpeg" alt="Hromádka časopisů">
</figure>

Celkem logicky jsem proto každé číslo následně založil do svého archívu. Byl to dobrý zdroj informací a zajímavých článků. Po pár letech jsem si však uvědomil, že sice archivuji, ale dál už s&#160;nimi nijak nepracuji. A v&#160;zásadě jsem ani nemohl&#160;– vždyť já už dávno zapomněl, co v&#160;těch číslech vlastně bylo! **A&#160;když nevím, že něco mám, není, jak bych to mohl využít.**

Mému archívu **chyběla strategie**. Nějaký plán. Tak jsem archív prošel, zajímavé nebo užitečné články vytrhal, zapsal je na seznam a zbytek vyhodil.

<figure class="post-image">
  <img src="/images/posts/archiv_casopisu.jpeg" alt="„Archív“ různých časopisů">
</figure>

Myslel jsem si, že to pomůže, že jsem tím problém vyřešil. Bohužel ne. Ani s&#160;tímto „mini archívem“ jsem nepracoval a nepamatoval si, co v&#160;něm je (a&#160;to ani když jsem měl papír se seznamem oněch článků&#160;– nevěděl jsem totiž, že bych se měl na ten seznam vůbec podívat, že tam je něco relevantního).

*S&#160;informacemi je třeba pracovat.* Skladovat materiály podle jejich původu mi obecně nedává smysl.

### Jak to dělám dnes
Poučil jsem se: **schraňovat časopisy prakticky nemá smysl**\*.

Při čtení používám zvýrazňovač na dobré pasáže textu, které stojí za další pozornost nebo potenciální uchování.

Následně pak projdu, co jsem si zvýraznil, a vypíši si informace do systému nebo na místo, kde mám materiály k&#160;danému tématu. Tento přístup mi funguje, protože tyto materiály běžně využívám, takže se mi nová informace z&#160;časopisu neztratí a nebude někde zapomenutá sbírat prach.

Časopis pak mohu vyhodit nebo ještě lépe poslat dál, protože co v&#160;něm bylo užitečné, už jsem vytěžil&#160;– skladovat ho tedy nemá žádný význam.

*) A výjimky potvrzují pravidlo. ;·)
