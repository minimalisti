---
title:  Můj první krok na cestě k minimalismu
author: Michal
---
Dá se říci, že k&#160;minimalismu jsem vykročil už někdy před 8&#160;lety. Tehdy jsem se poprvé začal aktivně zbavovat věcí, které jsem nechtěl a u&#160;kterých jsem ani nevěděl, co bych tak s&#160;nimi mohl dělat.

Ale nevyhazoval jsem je&#160;– tehdy jsem je vystavoval do aukcí na serveru Aukro.cz (od té doby zavedli poplatky za vystavování a celkově prodejnost věcí na Aukru klesla směrem k&#160;nule, takže už nějaké 4&#160;roky prodávám na serveru Sbazar.cz). Tam jsem s&#160;překvapením zjistil, že **i&#160;když mně přijde nějaká věc naprosto k&#160;ničemu, pro někoho jiného může být velice užitečná** (vč. třeba porouchané elektroniky, ale nejen té).

Prošel jsem teď všechny* záznamy o prodejích a zjistil, že od roku 2010 do dnešního dne (6.&#160;1.&#160;2019) jsem dal sbohem (pro mě) neuvěřitelným **483** věcem.

Celou dobu pozoruji zajímavou věc: kdykoli jsem se zbavil hromádky nepotřebných věcí, zakopl jsem o&#160;novou. Neznámo odkud se objevovaly další, další a další věci, kterých jsem se najednou chtěl zbavit. A jelikož šlo o&#160;věci, ke kterým jsem (už) neměl žádný vztah, nebylo obtížné se s&#160;nimi rozloučit.

Zajímavým vedlejším efektem, který jsem celou dobu přehlížel, je finanční stránka. Tím, že se rozhodnu něco už nemít, se mi nejen uvolní místo v&#160;životě pro důležitější věci, ale můžu částečně zrecyklovat i peníze, které jsem do těch věcí původně vložil.

Když se na seznam věcí, které už nemám, dnes podívám, jsem překvapený, kolik jich je, a děsí mě představa, že bych se jich nezbavil, ale dál je skladoval.

Jak vypadá vaše půda, sklep, skříně? Dává jejich obsah smysl?

---

*) Ve skutečnosti mi ještě jeden rok v&#160;záznamech chybí&#160;– tam procházím jednotlivé e-maily.
