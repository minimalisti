---
title: Naj 7 vecí na minimalizme
author: Ivan
excerpt: 1. Sloboda orientovať sa na budúcnosť a nie minulosť
---
1. Sloboda orientovať sa na budúcnosť a nie minulosť
2. Vnútorne sa cítim viac pohyblivý
3. Mám v izbe viac miesta
4. Údržba vecí zaberie tak akurát času
5. Teším sa zo života a z vecí
6. Mám voľnosť robiť to, čo považujem za dobrý nápad
7. Sťahovanie je oveľa ľahšie
