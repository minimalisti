---
title:  Ako upratať internetové kontá
author: Ivan
---
V&#160;súčasnosti, aby sme mohli pristupovať k&#160;mnohým stránkam na internete, máme každý spústu kônt. Desiatky až stovky. Myslím si, že aj tieto digitálne kontá majú svoj životný cyklus a je dobré ich po doslúžení odstrániť.

Najdôležitejšie je vyhodiť tie, ktoré obsahujú naše osobné informácie a žijú si veselo na službách „zadarmo“ (sociálne siete). Najmenej dôležité sú tie, kde máme meno a heslo a konto nie je zviazané ani s&#160;e-mailom (Battle.net classic).

A vždy sa dá zaregistrovať nové konto, nie? Sú chvíle, kedy naozaj chceme nový začiatok.

Ako odstraňovať svoje kontá zo systémov? V&#160;sekcii *profil* (profile) alebo *účet* zvykne bývať odkaz na odstránenie alebo aspoň deaktiváciu konta.

Ale často nebýva a pristupuje sa k&#160;nemu priamym odkazom zo sekcie nápovedy, čo je lepšie vyhľadať na nete. Stačí zadať niečo ako *delete yahoo account* a hneď vidno návody.

Účty na sociálnych sieťach (facebook, snapchat, icq) a obecne na veľkých službách západných spoločností (last.fm) sa mažú ľahko.

Nie všetko ale má návody a nie všade sa dá konto zmazať nejakým odkazom. Sú služby, kde treba poslať e-mail na adresu podpory (Path of Exile, Eve Online) či nás prepoja na agenta či živú podporu (EA, nVidia) (!?). S&#160;tým človekom v&#160;živom čete som nikdy nepísal, lebo som musel čakať na prepojenie. Čo sa mi nechcelo. Ale faktom je, že ani zďaleka neodpovie každý na poslaný email.

A nakoniec, sú stránky, kde sa nedá zmazať konto. Internetové fóra založené na phpBB a vBulletine sú týmto legendárne. Aj s Wordpressom sú problémy, ak sa nejaký šialenec rozhodne na tom postaviť fórum. A potom rôzne obskúrne služby s&#160;vlastným redakčným systémom a zlým technickým stavom.

A eshopy v&#160;Česku. Obzvlášť tie staré na oscommerce, zencarte a prestashope neumožňujú ľahké vyhodenie užívateľa. Ale je fajn, že vo väčšine obchodov, kde nakupujem, robím nákupy opakovane a konto si nechávam. Pri iných, ako army shopy, práve tieto majú vymakané systémy a pri kúpe sa netreba registrovať. Tiež by nemal byť problém poslať GDPR žiadosť o&#160;vymazanie.

Posledným druhom kônt sú také, ktoré poskytovateľ služby zmaže sám po určitej neaktivite (alphacoders.com po 2 rokoch). Toto je príjemné.

Keď som začal mazať kontá, ktoré som už nepoužíval či už mali zastaralý obsah a nevyjadrovali moje súčasné záľuby, tak som nebol z&#160;toho nadšený, bál som sa a nebol som si istý, či je to nutné.

Dnes s&#160;odstupom času som rozhodne rád, že sú preč, aj keď ma trochu mrzí, že som niekde zmazal obsah pre komunitu ako svoje obrázky a komentáre.

Beriem to tak, že nič nie je večné a veci proste zastarávajú a miznú. A ja sa túžim cítiť voľne a sústrediť sa na prítomnosť.
