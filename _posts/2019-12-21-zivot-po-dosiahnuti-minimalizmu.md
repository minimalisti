---
title:  Život po dosiahnutí minimalizmu
author: Ivan
---
Nedávno som si uvedomil, že som konečne zminimalizoval 99 percent vecí vo svojom živote. Trvalo to dlho, veľmi dlho. Netýkalo sa to len fyzických vecí, ale aj digitálnych súborov a aj činností spojené s&#160;nimi. Nie som si úplne istý, ako to Marie Kondo spravila za pár dní alebo týždňov. Tak píše vo svojej knihe. Mne to trvalo asi tak dva roky. Medzitým som nakupoval ďalšie veci a iných sa zbavoval. Po čase som sa zbavil aj väčšiny tých nových nakúpených. Teraz už zhruba viem, čo kupovať, čo sa mi naozaj páči, a nie to, čomu som veril, že by sa mi mohlo či malo páčiť.

Jedným, pre mňa, z&#160;najdôležitejších pocitov je pocit nudy a slobody. Nemusím nič robiť. Kedysi som zvykol konzumovať digitálny obsah a myslel si, že nemám na výber. Fyzické veci som kupoval skôr, aby som uvoľnil stres. Je úplne super len tak sedieť či ležať na posteli a rozmýšľať, čo chcem robiť. Mám slobodu zavrhnúť aj to, čo vlastne nechcem robiť. Je to obrovská zmena oproti minulosti, kde som sa riadil „musím prečítať tieto všetky technologické novinky, Facebook, Instagram, Snapchat, Twitter, odbery na YouTube“. A spústu iných vecí. Aby som náhodou nezostal pozadu a neuniklo mi niečo dôležité. Avšak nakoniec som zavrhol takýto spôsob prístupu k&#160;životu.

Ďalšou príjemnou vecou je sloboda zbavovať sa vecí, keď chcem. Ale nemusím, ak nechcem. Samozrejme, dá sa odvodzovať spoločenský status podľa vecí, ktoré vlastním. Ale taký prístup k&#160;životu je väzenie, z&#160;ktorého sa nedá utiecť. Nie som typ človeka, ktorý zbiera vecí a teší sa z&#160;nich. Som dobrodružnejší typ človeka a mobilita mi je milšia než kopa veci a nevidím na tom nič zlé.

Minimalizmom som odstránil väčšinu stresujúcich vecí vo svojom živote a môžem o&#160;dosť voľnejšie dýchať a regenerovať sa. To je presne to, po čom som dlho túžil. Stále vlastním veci a chodím na sociálne siete, ale už to nie je záležitosť života a smrti. V&#160;podstate na tom nezáleží, tak si to môžem užívať. Myslím si, že osobný život by nemal byť povinnosť, ale voľnosť. Už to nie je „musím hento, musím tamto“, ale „mohol by som hento, či tamto“.

Aj teraz je ľahké skĺznuť späť do nie-minimalizmu. Občas sa mi to stane, ale nejak sa netrápim. Väčšinou viem, čo ma k&#160;tomu viedlo a idem proste ďalej cestou minimalizmu.

Čo dodať na koniec? Trvalo to dlho, ale stojí to za to&#160;– výsledkom je pocit slobody, dobrá nálada a dobrý spánok.
