---
title:  Jedinečné veci
author: Ivan
---
Jedinečné veci. Ak je niečo, čomu mám problém odolávať, tak sú to ony. Vždy, keď vidím reklamu na rôzne unikátne veci alebo keď sa prehrabávam internetovým obchodom, tak dostanem nával typu „musím to mať!“.

Predsa je to jedinečná, neopakovateľná príležitosť a už sa fakt nikdy nebude opakovať. Nikdy. Zberateľské verzie hier, vinyly, alkohol vyrobený špeciálne k&#160;ôsmej sérii Game of Thrones, kusovo obmedzené tričká s&#160;motívom, či časovo obmedzené veci.

Na druhú stranu digitálna obdoba tohto princípu na mňa nefunguje. Časovo obmedzené veci do hier, exkluzivity pri predobjednávke či dostupnosť iba pre jednu platformu. Prvú vec mám na háku a zvyšné dve mi prídu vyslovene nefér a nemám potrebu sa ich zúčastňovať a podporovať ich hraním ich divadielka.

V&#160;digitálnom svete som proti týmto zvyklostiam, lebo nakopírovanie digitálnej veci väčšinou nič nestojí a tým pádom sa vytvára umelá obdoba nedostatku. A to len preto, aby bol nejaký obsah na PlayStatione, ktorý nie je na PC? Keby sa dal aspoň kúpiť…

Pri fyzických veciach to chápem, hodí sa obmedzený počet a nejaká cena. Ďalšie kusy niečo stoja.

Za posledné roky som si uchovával mnoho jedinečných súborov a kúpil jedinečné veci len preto, že boli jedinečné. Ale mňa nebaví mať veci vo vitríne. Zaberá to miesto a už tak nejak vyžadujem, aby som veci mohol používať. A zároveň musia mi byť okamžite sympatické v&#160;zmysle, že som ochotný ich nosiť každý deň alebo listovať v&#160;nich hocikedy.

Proste cítiť hlboký zmysel. V&#160;tomto ohľade som sa začal ovládať a skoro nič nekupujem v&#160;prvotnom návale, ale rozmyslím si to.

Jedinečné veci môžu získať časom na hodnote. Ale čo to stojí? Čas, priestor, peniaze. A hlavne narúša to moju osobnú integritu, lebo tomu neverím a záleží mi na tom. Proste minimalista.
