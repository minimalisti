#!/bin/sh -x
rm -rf _production/
bundle exec jekyll build -d _production
echo 'Press ENTER...'
read
rsync -cprtvz --delete _production/ kos:/srv/web/minimalisti.cz/www/
rm -rf _production/
