---
title:  "Minimalismus: dokumentární film o důležitých věcech – volně ke zhlédnutí"
author: Michal
---
Respektive v&#160;originále „Minimalism: A Documentary About the Important Things“.

Dva hlavní protagonisté filmu Joshua Fields Millburn a Ryan Nicodemus, známí také jako „The Minimalists“, zveřejnili letos celý film na svém invidious kanále, takže pokud jste ho ještě neviděli, pokračujte [tudy](https://invidious.fdn.fr/watch?v=J8DGjUv-Vjc). :·)

Jejich film byl mým [prvním seznámením s minimalismem](/2018/12/minimalisti-a-the-minimalists/) jako takovým a rozhodně ovlivnil můj vztah k&#160;věcem. A myslím si, že přehodnotit vztah k&#160;věcem potřebuje i celá naše společnost.
